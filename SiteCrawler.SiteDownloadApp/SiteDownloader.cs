﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using SiteCrawler.Lib;

namespace SiteCrawler.SiteDownloadApp
{
    public partial class SiteDownloader : Crawler
    {
        private const string ExtensionPage = ".html";

        private static readonly Regex regFileNameBadSmb = new Regex("[^a-zA-Z.0-9]+");

        private static readonly Dictionary<ResourceType, string> subfolderForResource = new Dictionary<ResourceType, string>
        {
            { ResourceType.Css, "css" }
        };

        public SiteDownloader(CrawlingOptions options, Action<string> logMessage, Action crawlingCompleted) : 
            base(options, logMessage, crawlingCompleted)
        {
            EnsureTargetFolder();
        }

        protected override void ProcessPage(string content, CrawlerMapNode node, List<CrawlerResource> pageLinks)
        {
            content = ReplacePageHyperlinksToFiles(content, node, pageLinks);
            if (options.downloadCss)
                content = DownloadAndReplaceCss(content, node);
            var fullPath = Path.Combine(options.targetFolder, 
                ResourseLinkToFileName(node.url.FullUrl) + ExtensionPage);
            StoreFile(fullPath, content);
        }

        private string ReplacePageHyperlinksToFiles(string content, CrawlerMapNode node, List<CrawlerResource> pageLinks)
        {
            foreach (var link in pageLinks)
            {
                var relUrl = node.url.CombineUrl(domain, link.sourceUri);
                if (!relUrl.sameDomain) continue;
                var fileName = ResourseLinkToFileName(relUrl.link);
                if (string.IsNullOrEmpty(fileName)) continue;
                var newUrl = $"{fileName}{ExtensionPage}";

                var linkEscaped = EscapeUrlRegexPattern(link.sourceUri);
                var regex = new Regex($"(?<=href\\s*=\\s*\")[{linkEscaped}]+(?=\")", RegexOptions.IgnoreCase);

                content = regex.Replace(content, newUrl);
            }

            return content;
        }

        private string ResourseLinkToFileName(string link)
        {
            CrawlerUrl uri;
            try
            {
                uri = new CrawlerUrl(link);
                var namePure = regFileNameBadSmb.Replace(uri.FullUrl, "_");
                return Md5Hash.ToMd5HashString(uri.UrlToFileName(domain)) + "_" + namePure;
            }
            catch
            {
                return string.Empty;
            }
        }

        private static string EscapeUrlRegexPattern(string url)
        {
            return Regex.Escape(url).Replace("-", "\\-");
        }

        private void StoreFile(string fullPath, string content)
        {
            using (var sw = new StreamWriter(fullPath, false, Encoding.UTF8))
                sw.Write(content);
        }

        private string UrlToFilePath(CrawlerUrl url)
        {
            var path = url.UrlToFileName(domain);
            return path;
        }

        private void EnsureTargetFolder()
        {
            options.targetFolder = string.IsNullOrEmpty(options.targetFolder)
                ? ExecutablePath.Combine("download")
                : options.targetFolder;
            if (Directory.Exists(options.targetFolder))
                ClearTargetFolder();
            else
                Directory.CreateDirectory(options.targetFolder);
            foreach (var folder in subfolderForResource.Values)
                Directory.CreateDirectory(Path.Combine(options.targetFolder, folder));
        }

        private void ClearTargetFolder()
        {
            var di = new DirectoryInfo(options.targetFolder);
            foreach (var file in di.GetFiles())
                file.Delete();
            foreach (var dir in di.GetDirectories())
                dir.Delete(true);
        }
    }
}
