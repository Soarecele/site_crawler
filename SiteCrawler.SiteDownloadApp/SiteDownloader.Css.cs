﻿using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using SiteCrawler.Lib;

namespace SiteCrawler.SiteDownloadApp
{
    public partial class SiteDownloader
    {
        private string DownloadAndReplaceCss(string content, CrawlerMapNode node)
        {
            var cssLinks = PageResourceFinder.FindLinksToCss(content);
            foreach (var link in cssLinks)
            {
                var relUrl = node.url.CombineUrl(domain, link.sourceUri);
                if (!relUrl.sameDomain) continue;

                // download (or ensure it was downloaded) the resourse
                var resFileName = LoadAndStoreResourse(link.sourceUri, relUrl, ResourceType.Css);
                if (string.IsNullOrEmpty(resFileName))
                    continue;

                // change the reference
                var newUrl = "file:///" + ExecutablePath.Combine(options.targetFolder,
                    subfolderForResource[ResourceType.Css], resFileName);

                var linkEscaped = EscapeUrlRegexPattern(link.sourceUri);
                var regex = new Regex($"(?<=href\\s*=\\s*\")[{linkEscaped}]+(?=\")", RegexOptions.IgnoreCase);

                content = regex.Replace(content, newUrl);
            }
            return content;
        }

        private string LoadAndStoreResourse(string sourceUrl, CombinedUrl relUrl, ResourceType resType)
        {
            var fileName = ResourseLinkToFileName(relUrl.link);
            if (resourceLib.CheckExistAndStore(resType, fileName))
                return fileName;

            var css = PageLoader.LoadPageContent(relUrl.link, out var error);
            if (options.verbose && !string.IsNullOrEmpty(error))
                logMessage($"File [{sourceUrl}] was not loaded: {error}");
            if (string.IsNullOrEmpty(css))
                return string.Empty;
            
            var fullPath = Path.Combine(options.targetFolder, subfolderForResource[resType], fileName);
            using (var sw = new StreamWriter(fullPath, false, Encoding.UTF8))
                sw.Write(css);
            return fileName;
        }
    }
}
