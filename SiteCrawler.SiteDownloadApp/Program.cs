﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using SiteCrawler.Lib;

namespace SiteCrawler.SiteDownloadApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var options = ReadOptions();
            var downloader = new SiteDownloader(options, 
                Console.WriteLine, () => Console.WriteLine("site downloading completed"));
            downloader.Start();

            Console.ReadLine();
        }

        private static CrawlingOptions ReadOptions()
        {
            string json;
            using (var sr = new StreamReader(ExecutablePath.Combine("options.json"), Encoding.UTF8))
                json = sr.ReadToEnd();
            return JsonConvert.DeserializeObject<CrawlingOptions>(json);
        }
    }
}
