﻿using NUnit.Framework;
using SiteCrawler.Lib;

namespace SiteCrawler.TestLive
{
    [TestFixture]
    public class TestPageLoader
    {
        [Test]
        public void TestOk()
        {
            var content =
                PageLoader.LoadPageContent("https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/index",
                    out var error);
            Assert.Greater(content.Length, 100);
            Assert.IsTrue(string.IsNullOrEmpty(error));
        }

        [Test]
        public void TestNotExisted()
        {
            var content =
                PageLoader.LoadPageContent("http://contoso.com/basme/mirceacartarescu",
                    out var error);
            Assert.IsTrue(string.IsNullOrEmpty(content));
            Assert.IsFalse(string.IsNullOrEmpty(error));
        }
    }
}
