﻿using System.Collections.Generic;
using NUnit.Framework;
using SiteCrawler.Lib;

namespace SiteCrawler.TestLive
{
    [TestFixture]
    public class TestCrawlerMsdn
    {
        [OneTimeSetUp]
        public void Setup()
        {
            ExecutablePath.InitializeFake(string.Empty);
        }

        [Test]
        public void Test()
        {
            var options = new CrawlingOptions
            {
                startUrl = "https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/index",
                maxLevel = 3,
                maxPagesTotal = 14
            };
            var messages = new List<string>();
            var completed = false;

            var crawler = new Crawler(options, m => messages.Add(m), () => completed = true);
            crawler.Start();
        }
    }
}
