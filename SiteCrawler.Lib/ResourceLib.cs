﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SiteCrawler.Lib
{
    /// <summary>
    /// holds a collection of page external links by type:
    /// - other links, css, img, scripts
    /// </summary>
    public class ResourceLib
    {
        private readonly Dictionary<ResourceType, HashSet<string>> downloadedResources;

        public ResourceLib()
        {
            downloadedResources = Enum.GetValues(typeof(ResourceType)).Cast<ResourceType>()
                .ToDictionary(t => t, t => new HashSet<string>());
        }

        public bool CheckExistAndStore(ResourceType resTp, string resourceFileName)
        {
            var hash = downloadedResources[resTp];
            if (hash.Contains(resourceFileName))
                return true;
            hash.Add(resourceFileName);
            return false;
        }
    }

    public class CrawlerResource
    {
        public ResourceType resourceType;

        public string sourceUri;

        public override string ToString()
        {
            return $"[{resourceType}]: {sourceUri}";
        }
    }

    public enum ResourceType
    {
        Link = 0, Css, Img, Script
    }
}
