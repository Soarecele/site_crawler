﻿using System;
using System.IO;
using System.Net;

namespace SiteCrawler.Lib
{
    /// <summary>
    /// load URL's content as a single string
    /// 
    /// Of course, it would be better to browse the page char-by-char
    /// But I need the whole content to pass through regular expressions
    /// </summary>
    public static class PageLoader
    {
        public static string LoadPageContent(string url, out string error)
        {
            error = "";
            try
            {
                var req = WebRequest.Create(url);
                var resp = req.GetResponse();

                string content;
                using (var stream = resp.GetResponseStream())
                using (var sr = new StreamReader(stream))
                    content = sr.ReadToEnd();

                return content;
            }
            catch (Exception e)
            {
                error = $"{e.GetType().Name}: {e.Message}";
                return string.Empty;
            }
        }
    }
}
