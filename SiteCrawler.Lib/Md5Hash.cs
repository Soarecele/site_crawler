﻿using System.Security.Cryptography;
using System.Text;

namespace SiteCrawler.Lib
{
    /// <summary>
    /// this class I have copy-pasted from my other project
    /// so I won't test it in this solution
    /// </summary>
    public static class Md5Hash
    {
        public static string ToMd5HashString(string src)
        {
            if (string.IsNullOrEmpty(src)) return string.Empty;

            var _md5 = new MD5CryptoServiceProvider();
            var bytes = Encoding.UTF8.GetBytes(src);
            var bytesHash = _md5.ComputeHash(bytes);

            var result = new StringBuilder(bytesHash.Length * 2);
            foreach (var t in bytesHash)
                result.Append(t.ToString("x2"));
            return result.ToString();
        }
    }
}
