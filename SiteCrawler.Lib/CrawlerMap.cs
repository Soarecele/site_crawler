﻿using System.Collections.Generic;

namespace SiteCrawler.Lib
{
    /// <summary>
    /// a tree and a hashset of urls downloaded
    /// hashset is used for not to download page twice
    /// </summary>
    public class CrawlerMap
    {
        public int TotalPagesCrawled { get; private set; }

        public int TotalPagesLoaded { get; private set; }

        public CrawlerMapNode root;

        private HashSet<string> crawledNodes = new HashSet<string>();

        // returns: child node or null
        // null if page was already browsed
        public CrawlerMapNode CrawlUrl(CrawlerMapNode parent, CrawlerUrl url)
        {
            if (crawledNodes.Contains(url.FullUrl))
                return null;
            var level = parent == null ? 0 : parent.level + 1;
            var node = new CrawlerMapNode(url, level);
            if (parent == null)
                root = node;
            else
                parent.children.Add(node);
            crawledNodes.Add(url.FullUrl);
            TotalPagesCrawled++;
            return node;
        }

        public void IncrementPagesLoaded()
        {
            TotalPagesLoaded++;
        }
    }

    public class CrawlerMapNode
    {
        public readonly CrawlerUrl url;

        public readonly int level;

        public bool pageLoaded;

        public List<CrawlerMapNode> children = new List<CrawlerMapNode>();

        public CrawlerMapNode(CrawlerUrl url, int level)
        {
            this.url = url;
            this.level = level;
        }

        public override string ToString()
        {
            var suffix = pageLoaded ? "" : ", not loaded";
            return $"[{url}]:{level}{suffix}";
        }
    }
}
