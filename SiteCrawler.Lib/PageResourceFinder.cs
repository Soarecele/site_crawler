﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SiteCrawler.Lib
{
    /// <summary>
    /// the class goes through a page content
    /// and finds all resources (see ResourceType, CrawlerResource)
    /// </summary>
    public static class PageResourceFinder
    {
        private static readonly Regex regHyperlink = new Regex("<a[^>]+href=[^>]+[^>]+>", RegexOptions.IgnoreCase);

        private static readonly Regex regHref = new Regex("(?<=href\\s*=\\s*\")[^\"]+(?=\")", RegexOptions.IgnoreCase);

        private static readonly Regex regCssLink = new Regex("<link[^>]+href\\s*=\\s*\"[^\"]*.css[^\"]*\"", RegexOptions.IgnoreCase);

        public static List<CrawlerResource> FindUrls(string content)
        {
            var links = regHyperlink.Matches(content).Cast<Match>().Select(m => m.Value);
            var refs = links.Select(l => regHref.Match(l).Value).Where(s => !string.IsNullOrEmpty(s) &&
                !s.StartsWith("#") && !s.StartsWith("{")).ToList();
            return refs.Select(r => new CrawlerResource
            {
                resourceType = ResourceType.Link,
                sourceUri = r
            }).ToList();
        }

        public static List<CrawlerResource> FindLinksToCss(string content)
        {
            var links = regCssLink.Matches(content).Cast<Match>().Select(m => m.Value);
            var refs = links.Select(l => regHref.Match(l).Value).Where(s => !string.IsNullOrEmpty(s)).ToList();
            return refs.Select(r => new CrawlerResource
            {
                resourceType = ResourceType.Css,
                sourceUri = r
            }).ToList();
        }
    }
}
