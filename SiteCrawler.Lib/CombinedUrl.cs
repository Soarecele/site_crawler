﻿namespace SiteCrawler.Lib
{
    public class CombinedUrl
    {
        public string link;

        public string originalLink;

        public CombinedUrlType urlType;

        public bool sameDomain = true;

        public CombinedUrl()
        {
        }

        public CombinedUrl(string originalLink, string link, CombinedUrlType urlType)
        {
            this.originalLink = originalLink;
            this.link = link;
            this.urlType = urlType;
        }

        public CombinedUrl(string originalLink, string link, CombinedUrlType urlType, bool sameDomain)
        {
            this.originalLink = originalLink;
            this.link = link;
            this.urlType = urlType;
            this.sameDomain = sameDomain;
        }

        public override string ToString()
        {
            var suffix = sameDomain ? ", same domain" : "";
            return $"{link} [{urlType}{suffix}]";
        }
    }

    public enum CombinedUrlType
    {
        Relative = 0,
        Absolute
    }
}
