﻿namespace SiteCrawler.Lib
{
    public class CrawlingOptions
    {
        public string startUrl;

        public string domain;

        public int maxLevel;

        public int maxPagesTotal;

        public string targetFolder;

        public bool verbose;

        public bool downloadCss;
    }
}
