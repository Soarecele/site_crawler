﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SiteCrawler.Lib
{
    public class Crawler
    {
        public readonly CrawlerUrl startUrl;

        public readonly CrawlerUrl domain;

        public readonly CrawlingOptions options;

        public readonly CrawlerMap map;

        public readonly ResourceLib resourceLib;

        protected readonly Action<string> logMessage;

        private readonly Action crawlingCompleted;

        protected volatile bool breakFlag;

        public Crawler(CrawlingOptions options, 
            Action<string> logMessage,
            Action crawlingCompleted)
        {
            if (string.IsNullOrEmpty(options.startUrl))
                throw new ArgumentException("Crawler.ctor: startUrl is nil");

            this.options = options;
            this.logMessage = logMessage;
            this.crawlingCompleted = crawlingCompleted;
            startUrl = new CrawlerUrl(options.startUrl);
            options.domain = string.IsNullOrEmpty(options.domain) ? startUrl.Domain : options.domain;
            
            domain = new CrawlerUrl(options.domain);
            map = new CrawlerMap();
            resourceLib = new ResourceLib();
        }

        public void Start()
        {
            logMessage($"Started crawling website [{startUrl.FullUrl}],");
            logMessage($"domain is [{startUrl.Domain}]");

            BrowsePage(null, startUrl);

            crawlingCompleted();
        }

        public void Break()
        {
            breakFlag = true;
        }

        private void BrowsePage(CrawlerMapNode parent, CrawlerUrl url)
        {
            if (breakFlag) return;
            var node = map.CrawlUrl(parent, url);
            if (node == null)
            {
                // the page was probably already crawled
                return;
            }

            var content = PageLoader.LoadPageContent(url.url, out var error);
            if (string.IsNullOrEmpty(content))
            {
                if (options.verbose)
                    logMessage($"Page [{url}] was not loaded: {error}");
                return;
            }
            if (options.verbose)
                logMessage($"Page [{url}] is loaded [lv{node.level}, {map.TotalPagesLoaded}/{options.maxPagesTotal}]");
            map.IncrementPagesLoaded();
            if (breakFlag) return;

            // page hyperlinks that probably should be replaced to file links
            var rawLinks = PageResourceFinder.FindUrls(content);

            // do something with the page in a derived class
            ProcessPage(content, node, rawLinks);

            // delve into ... but not too deep
            if ((parent?.level ?? 0) >= options.maxLevel - 1)
                return;
            var siteLinks = rawLinks.Select(l => url.CombineUrl(domain, l.sourceUri)).
                Where(l => l.sameDomain).ToList();
            foreach (var link in siteLinks)
            {
                if (map.TotalPagesLoaded >= options.maxPagesTotal) break;
                BrowsePage(node, new CrawlerUrl(link.link));
            }
        }

        protected virtual void ProcessPage(string content, CrawlerMapNode node, List<CrawlerResource> pageLinks)
        {
        }        
    }
}
