﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace SiteCrawler.Lib
{
    /// <summary>
    /// the class splits a URL into parts: domain, page name, query string
    /// </summary>
    public class CrawlerUrl
    {
        private static readonly Regex regAnchor = new Regex("#.+$");

        public readonly string url;

        public Uri Uri { get; }

        public string Host => Uri.Host;

        public string FullUrl => Uri.AbsoluteUri;

        public string Domain => 
            Uri.IsDefaultPort ? $"{Uri.Scheme}://{Uri.Host}" :
                $"{Uri.Scheme}://{Uri.Host}:{Uri.Port}";

        public CrawlerUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                throw new ArgumentException("CrawlerUrl.ctor(nil)");
            this.url = UniformUrl(url);
            Uri = new Uri(this.url);
        }

        public CombinedUrl CombineUrl(CrawlerUrl domainUrl, string link)
        {
            var original = link;
            link = link.TrimEnd('/');
            try
            {
                // abs path like "https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/index"
                var uri = new CrawlerUrl(link);
                // -> "https://docs.microsoft.com/previous-versions/" (host + absoluteUri + absolutePath!=host)
                var sameHost = uri.Domain.Equals(domainUrl.Domain, StringComparison.InvariantCultureIgnoreCase);
                return new CombinedUrl(original, link, CombinedUrlType.Absolute, sameHost);
            }
            catch
            {
            }

            // "/ru-ru/dotnet/api/system.runtime.gcsettings" or simple "/profile"
            if (link.StartsWith("/"))
                return new CombinedUrl(original, domainUrl.FullUrl.TrimEnd('/') + "/" + link.Trim('/'), CombinedUrlType.Relative);
            
            // "performance"
            var sourceParts = FullUrl.Split('/');
            if (sourceParts.Length == 1)
                link = $"{FullUrl.TrimEnd('/')}/{link}";
            else
                link = string.Join("/", sourceParts.Take(sourceParts.Length - 1)).TrimEnd('/') + "/" + link;
            return new CombinedUrl(original, link, CombinedUrlType.Relative);
        }

        public string UrlToFileName(CrawlerUrl domainUrl)
        {
            var path = EncodePath(Uri.AbsolutePath);
            var queryPart = string.IsNullOrEmpty(Uri.Query) ? "" : EncodeQueryString(Uri.Query);
            return $"{path}{queryPart}";
        }

        public override string ToString()
        {
            return FullUrl;
        }

        private string UniformUrl(string url)
        {
            return regAnchor.Replace(url, "");
        }

        private static string EncodeQueryString(string query)
        {
            return "[" + query.Trim('/').Trim('?').Replace("&", "#").Replace("\\", "_sl_") + "]";
        }

        private static string EncodePath(string query)
        {
            return query.Trim('/').Replace("&", "#").Replace("\\", "_sl_").Replace("/", "#");
        }
    }
}
