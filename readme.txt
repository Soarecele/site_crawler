I. Task

Solution SiteCrawler solves the provided task:
=============================================================
Please develop a generic site crawler library, that provides basic services
to traverse a complete site tree.

Given a start link it visits each page of the site, i.e. all pages that are reachable 
via one or more hops from the startpage within the same domain. 
The caller can execute an arbitrary action on each of the pages.

Deliverables:

1) Develop a basic crawler component as described above

2) A sample console application that uses the crawler component to 
save all site pages for a given url as static files to the file system.
=============================================================

II. Solution structure

SiteCrawler.Lib - main solution�s library. A basic crawler component resides here in the file Crawler.cs (deliverables - 1)

SiteCrawler.SiteDownloadApp - a testing console app (deliverables - 2). Contains class SiteDownloader, derived from Crawler class.

SiteCrawler.Test - a test project (NUnit) for SiteCrawler.Lib project.

SiteCrawler.SiteDownloadApp.Test - an empty (unfinished) project for unit testing SiteDownloader component�s functionality.

SiteCrawler.TestLive - a project for �integration� tests, i.e. tests, that address Internet in my case.

III. How to execute

Run the \SiteCrawler.SiteDownloadApp\bin\Debug\SiteCrawler.SiteDownloadApp.exe either from solution bin folder or from the zip file attached (in the solution�s root).

The �crawler� would download the website as it is defined in options.json file next to .exe.

IV. Program settings

File options.json contains the following:

  "startUrl" - a path (URL) to target site that is to be downloaded.
  "domain" - should be left untouched (empty) for most scenarios. Used to resolve relative paths.
  "maxLevel" - maximum recursion level for browsing website hyperlinks.
  "maxPagesTotal" - maximum files count. When the value is reached, the search will stop.
  "targetFolder" - target folder on the hard drive, full path. If it is not set, would be �download� subfolder in the program�s directory
  "verbose" (true / false) - for more or less detailed logs
  "downloadCss" - if true, the program also download css files by the visited pages� hyperlinks.

V. What else could be done

Unfortunately, I could not finish the following due to the time limitations:
5.1) the website downloading code itself is not covered by test as it should have been.
5.2) *.js files and images are not loaded
5.3) it would be great to transform relative hyperlinks to absolute ones - for those links that are not being downloaded
5.4) a website tree is better to be traversed in level-order. That is, the program should not delve into by 3-d level links while not all the 2-d level links are explored. Faced this challenge but didn�t manage to solve it in time.
