﻿using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using SiteCrawler.Lib;

namespace SiteCrawler.Test
{
    [TestFixture]
    public class TestPageResourceFinder
    {
        [OneTimeSetUp]
        public void Setup()
        {
            ExecutablePath.InitializeFake(string.Empty);
        }

        [Test]
        public void TestUrls()
        {
            var content = ReadFileContent("sample_msdn.html");
            var links = PageResourceFinder.FindUrls(content);
            Assert.Greater(links.Count, 10);
        }

        [Test]
        public void TestCss()
        {
            var content = ReadFileContent("sample_msdn.html");
            var links = PageResourceFinder.FindLinksToCss(content);
            Assert.Greater(links.Count, 1);
            Assert.IsTrue(links.All(l => l.sourceUri.Contains(".css")));
        }

        private string ReadFileContent(string fileName)
        {
            var path = ExecutablePath.Combine("files", fileName);
            using (var sr = new StreamReader(path, Encoding.UTF8))
                return sr.ReadToEnd();
        }
    }
}
