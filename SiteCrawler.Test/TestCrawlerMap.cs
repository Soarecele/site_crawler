﻿using NUnit.Framework;
using SiteCrawler.Lib;

namespace SiteCrawler.Test
{
    [TestFixture]
    public class TestCrawlerMap
    {
        [Test]
        public void TestCorrectMap()
        {
            var map = new CrawlerMap();
            var root = map.CrawlUrl(null, new CrawlerUrl("https://www.contoso.com:8080/help"));
            Assert.AreEqual(root.url.FullUrl, map.root.url.FullUrl);

            var branchA = map.CrawlUrl(root, new CrawlerUrl("https://www.contoso.com:8080/help/about"));
            var branchB = map.CrawlUrl(root, new CrawlerUrl("https://www.contoso.com:8080/help/tools"));

            var linkC = map.CrawlUrl(branchA, new CrawlerUrl("https://www.contoso.com:8080/help/about/authors.html"));
            var linkD = map.CrawlUrl(branchA, new CrawlerUrl("https://www.contoso.com:8080/help/about/reference.html"));

            map.CrawlUrl(root, new CrawlerUrl("https://www.contoso.com:8080/help/tools"));
            map.CrawlUrl(branchA, new CrawlerUrl("https://www.contoso.com:8080/help/about/reference.html#pageAnchor1"));

            Assert.AreEqual(0, root.level);
            Assert.AreEqual(1, branchA.level);
            Assert.AreEqual(1, branchB.level);

            Assert.AreEqual(2, linkC.level);
            Assert.AreEqual(2, linkD.level);

            Assert.AreEqual(2, root.children.Count);
            Assert.AreEqual(2, branchA.children.Count);

            Assert.AreEqual(5, map.TotalPagesCrawled);
        }
    }
}
