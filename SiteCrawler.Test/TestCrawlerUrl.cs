﻿using System.Linq;
using NUnit.Framework;
using SiteCrawler.Lib;

namespace SiteCrawler.Test
{
    [TestFixture]
    public class TestCrawlerUrl
    {
        [Test]
        public void TestDomain()
        {
            var url = new CrawlerUrl("https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/index");
            Assert.AreEqual("docs.microsoft.com", url.Host);
            Assert.AreEqual("https://docs.microsoft.com", url.Domain);

            url = new CrawlerUrl("https://docs.microsoft.com:8080/ru-ru/dotnet/standard/garbage-collection/index");
            Assert.AreEqual("https://docs.microsoft.com:8080", url.Domain);
        }

        [Test]
        public void TestUniform()
        {
            var urlA = new CrawlerUrl("https://contoso.com/en/index?search=gc&global=false#someAnchor-1");
            var urlB = new CrawlerUrl("https://contoso.com/en/index?search=gc&global=false#B");
            var urlC = new CrawlerUrl("https://contoso.com/en/index?search=gc&global=false");
            Assert.AreEqual(urlA.FullUrl, urlB.FullUrl);
            Assert.AreEqual(urlA.FullUrl, urlC.FullUrl);

            var urlD = new CrawlerUrl("https://contoso.com/en/index?search=gc&global=ALPHA");
            var urlE = new CrawlerUrl("https://CONTOSO.com/en/index?search=gc&global=ALPHA");
            var urlF = new CrawlerUrl("https://CONTOSO.com/en/index?search=gc&global=alpha");
            Assert.AreEqual(urlD.FullUrl, urlE.FullUrl);
            Assert.AreNotEqual(urlE.FullUrl, urlF.FullUrl);
        }

        [Test]
        public void CheckUrlCombine()
        {
            var originLinks = new[]
            {
                "https://github.com/dotnet/docs.ru-ru/blob/live-sxs/docs/standard/garbage-collection/index.md",
                "/profile", "https://github.com/rpetrusha",
                "https://github.com/olprod", "https://github.com/OpenLocalizationService", "fundamentals",
                "performance", "induced", "latency",
                "optimization-for-shared-web-hosting", "notifications", "app-domain-resource-monitoring",
                "weak-references", "/ru-ru/dotnet/api/system.gc",
                "/ru-ru/dotnet/api/system.gccollectionmode", "/ru-ru/dotnet/api/system.gcnotificationstatus",
                "/ru-ru/dotnet/api/system.runtime.gclatencymode",
                "/ru-ru/dotnet/api/system.runtime.gcsettings",
                "/ru-ru/dotnet/api/system.runtime.gcsettings.largeobjectheapcompactionmode",
                "/ru-ru/dotnet/api/system.object.finalize", "/ru-ru/dotnet/api/system.idisposable", "unmanaged",
                "https://developercommunity.visualstudio.com/spaces/61/index.html",
                "https://docs.microsoft.com/teamblog/a-new-feedback-system-is-coming-to-docs",
                "https://github.com/dotnet/docs.ru-ru/issues",
                "https://docs.microsoft.com/previous-versions/", "https://docs.microsoft.com/teamblog",
                "https://docs.microsoft.com/contribute",
                "https://go.microsoft.com/fwlink/?LinkId=521839", "/ru-ru/legal/termsofuse",
                "https://aka.ms/sitefeedback",
                "https://www.microsoft.com/en-us/legal/intellectualproperty/Trademarks/EN-US.aspx",
                "https://docs.microsoft.com/previous-versions/",
                "https://docs.microsoft.com/teamblog", "https://docs.microsoft.com/contribute",
                "https://go.microsoft.com/fwlink/?LinkId=521839",
                "/ru-ru/legal/termsofuse", "https://aka.ms/sitefeedback",
                "https://www.microsoft.com/en-us/legal/intellectualproperty/Trademarks/EN-US.aspx"
            };
            var siteUrl = new CrawlerUrl("https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/index");
            var domainUrl = new CrawlerUrl(siteUrl.Domain);

            var urls = originLinks.Select(l => siteUrl.CombineUrl(domainUrl, l)).ToList();
            Assert.LessOrEqual(urls.Count, originLinks.Length);
            Assert.Greater(urls.Count, 20);
            Assert.IsTrue(urls.Any(u => u.sameDomain));
            Assert.IsTrue(urls.Any(u => !u.sameDomain));
            Assert.IsTrue(urls.Any(u => u.urlType == CombinedUrlType.Absolute));
            Assert.IsTrue(urls.Any(u => u.urlType == CombinedUrlType.Relative));
        }

        [Test]
        public void TestUrlToPath()
        {
            var siteUrl = new CrawlerUrl("https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/index");
            var domainUrl = new CrawlerUrl(siteUrl.Domain);
            var pathA = siteUrl.UrlToFileName(domainUrl);

            siteUrl = new CrawlerUrl("https://docs.microsoft.com/ru-ru/dotnet/standard/garbage-collection/index?item=gc&value=full");
            var pathB = siteUrl.UrlToFileName(domainUrl);

            Assert.Greater(pathA.Length, 5);
            Assert.Greater(pathB.Length, pathA.Length);
        }
    }
}
